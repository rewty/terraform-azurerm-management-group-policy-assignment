<!-- BEGIN_TF_DOCS -->

# A simplified version of CAF Enterprise Scale used to assign policies

## Usage

A defintion file, known as an 'archetype' is used to simplify the application of policies to management groups of a similar 'type'.

For example, an archetype file might be defined that specifies policies for a 'Sandbox' Management Group. This would require very strict policies for the type (& tier) of resources deployed along with strict limitations of how resources interact with other environments. In this case, an archetype would be defined with policies declared to enforce these standards.

The benefit of using these 'types', is that a management group can be assigned to a single archetype type and receive all policies defined. This reduces the amount of duplication needed to assign policies.

However, this may reduce flexibility. If a Management Group requires similar, but not the same, set of policies defined in an Archetytpe, then a new archetype will be required.

The use of Archetypes, and the method for defining them in code as part of the Terraform deployment, is defined in the Microsoft 'CAF' Terraform modules. Where an Enterprise Scale Azure infrastructure can be deployed using Terraform. While the Microsoft CAF module achieves the deployment of a large, Enterprise Scale Azure environment, the code itself is quite complex and in many cases, not required for smaller environments.

```terraform
module "management_group_policy_assignment_root" {
  source = "./modules/terraform-azurerm-management-group-policy-assignment"

  # which archetype this assignment is a member of
  archetype            = "root"
  # where to apply this assignment
  management_group     = "corp"
  # files in this directory must match the policy_assignments section of archetype configs
  assignment_file_path = "${path.module}/lib/policyAssignments"
  # define archetypes of management groups and match to name of policyAssignment .json files
  archetype_files_path = "${path.module}/lib/types"
}
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | >= 2.46.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | >= 2.46.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azurerm_management_group_policy_assignment.assignment](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/management_group_policy_assignment) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_archetype"></a> [archetype](#input\_archetype) | The archetype assigned to the Management Group. Only archetype definitions defined in this repo (./types/) can be used. | `string` | n/a | yes |
| <a name="input_archetype_files_path"></a> [archetype\_files\_path](#input\_archetype\_files\_path) | description | `string` | n/a | yes |
| <a name="input_assignment_file_path"></a> [assignment\_file\_path](#input\_assignment\_file\_path) | The path to the Policy Assignment files. | `string` | n/a | yes |
| <a name="input_management_group"></a> [management\_group](#input\_management\_group) | The Management Group where policies are to be assigned. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_assignment_id"></a> [assignment\_id](#output\_assignment\_id) | n/a |
<!-- END_TF_DOCS -->