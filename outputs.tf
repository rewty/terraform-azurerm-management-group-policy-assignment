output "assignment_id" {
  value = tomap({
    for k, pa in azurerm_management_group_policy_assignment.assignment : k => pa.id
  })
}
